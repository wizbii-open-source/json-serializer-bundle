[![pipeline status](https://gitlab.com/wizbii-open-source/json-serializer-bundle/badges/master/pipeline.svg)](https://gitlab.com/wizbii-open-source/json-serializer-bundle/commits/master)
[![coverage report](https://gitlab.com/wizbii-open-source/json-serializer-bundle/badges/master/coverage.svg)](https://gitlab.com/wizbii-open-source/json-serializer-bundle/commits/master)

Objectives
----------

This package aims to help you serialize and deserialize objects into JSON with maximum performances. To do so, no reflection is used and the developer must implement 2 methods for each objects.
It depends on php 7.4+ and Symfony 5.0+

Installation
------------

`composer require wizbii/json-serializer-bundle`

Usage
-----

First, create an object that implement `ArraySerializable` interface:

```$php
use Wizbii\JsonSerializerBundle\ArraySerializable;

class SimpleSerializableObject implements ArraySerializable
{
    private string $foo;

    public function serialize(): array
    {
        return [
            'foo' => $this->foo,
        ];
    }

    public static function deserialize(array $contentAsArray)
    {
        return (new SimpleSerializableObject())->setFoo($contentAsArray['foo'] ?? '');
    }

    public function getFoo(): string
    {
        return $this->foo;
    }

    public function setFoo(string $foo): SimpleSerializableObject
    {
        $this->foo = $foo;

        return $this;
    }
}
```

Then, use the `Serializer` service to (de)serialize it: 

```$php
class MyController
{
    private Serializer $serializer;
    
    public function __construct(Serializer $serializer) {
        $this->serializer = $serializer;
    }

    public function getSimpleObjectAction() {
        $simpleObject = (new SimpleObject())->setFoo('bar');
        return new Response($this->serializer->serialize($simpleObject));
    }

    public function getFoo() {
        $content = '{"foo":"bar"}';
        $simpleObject = $this->serializer->deserialize($content, SimpleObject::class);
        return new Response($simpleObject->getFoo());
    } 
}
```

Contribute
----------

1. Fork the repository
1. Make your changes
1. Test them with `composer dev:checks` (it will run test, phpstan and cs:lint subcommands)
1. Create a Merge Request
