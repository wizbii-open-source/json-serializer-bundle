<?php

namespace Tests\Wizbii\JsonSerializerBundle;

use Tests\Wizbii\JsonSerializerBundle\Fixture\SimpleSerializableObject;

class SimpleModelTest extends ModelTestCase
{
    protected function getSystemUnderTestClassName(): string
    {
        return SimpleSerializableObject::class;
    }

    protected function getTestFileBasePath(): string
    {
        return __DIR__.'/resources/simple_serializable_object';
    }
}
