<?php

namespace Tests\Wizbii\JsonSerializerBundle\Fixture;

class NotSerializableObject
{
    private string $foo;

    public function getFoo(): string
    {
        return $this->foo;
    }

    public function setFoo(string $foo): NotSerializableObject
    {
        $this->foo = $foo;

        return $this;
    }
}
