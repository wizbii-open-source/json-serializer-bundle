<?php

namespace Tests\Wizbii\JsonSerializerBundle\Fixture;

use Wizbii\JsonSerializerBundle\ArraySerializable;

class SimpleSerializableObject implements ArraySerializable
{
    private string $foo;

    public function serialize(): array
    {
        return [
            'foo' => $this->foo,
        ];
    }

    public static function deserialize(array $contentAsArray): static
    {
        return (new SimpleSerializableObject())->setFoo($contentAsArray['foo'] ?? '');
    }

    public function getFoo(): string
    {
        return $this->foo;
    }

    public function setFoo(string $foo): SimpleSerializableObject
    {
        $this->foo = $foo;

        return $this;
    }
}
