<?php

namespace Tests\Wizbii\JsonSerializerBundle;

use PHPUnit\Framework\Constraint\IsEqual;
use PHPUnit\Framework\TestCase;
use Tests\Wizbii\JsonSerializerBundle\Fixture\Faker;
use Wizbii\JsonSerializerBundle\ArraySerializable;
use Wizbii\JsonSerializerBundle\Exception\DeserializationNotSupportedException;
use Wizbii\JsonSerializerBundle\Exception\SerializationNotSupportedException;
use Wizbii\JsonSerializerBundle\Serializer;

abstract class ModelTestCase extends TestCase
{
    protected Serializer $serializer;

    protected array $testedModels = ['regular'];
    protected array $ignoredPropertiesForAccessorTest = [];
    /** @var array<string, callable> */
    private array $dataTypeProviders = [];
    protected bool $isSerializationTestable = true;
    protected bool $isDeserializationTestable = true;

    private array $includeCache = [];

    abstract protected function getSystemUnderTestClassName(): string;

    abstract protected function getTestFileBasePath(): string;

    public function setup(): void
    {
        parent::setup();
        $this->serializer = new Serializer();
        date_default_timezone_set('Europe/Paris');
        $this
            ->addDataTypeProvider('int', fn () => Faker::integer())
            ->addDataTypeProvider('float', fn () => Faker::float())
            ->addDataTypeProvider('bool', fn () => Faker::bool())
            ->addDataTypeProvider('string', fn () => Faker::string())
            ->addDataTypeProvider('array', fn () => Faker::array())
            ->addDataTypeProvider(\DateTime::class, fn () => Faker::dateTime())
            ->addDataTypeProvider(\DateTimeImmutable::class, fn () => Faker::dateTimeImmutable())
        ;
    }

    protected function addDataTypeProvider(string $type, callable $callable): self
    {
        $this->dataTypeProviders[$type] = $callable;

        return $this;
    }

    /**
     * @dataProvider getObjectsAndFeeds
     */
    public function testItCanSerializeObject(ArraySerializable $object, string $expectedFeed)
    {
        if (!$this->isSerializationTestable) {
            $this->expectException(SerializationNotSupportedException::class);
            $this->serializer->serialize($object);
        }
        $serializedFeed = $this->serializer->serialize($object);
        $this->assertThat($serializedFeed, $this->equalTo($expectedFeed));
    }

    /**
     * @dataProvider getObjectsAndFeeds
     */
    public function testItCanDeserializeText(ArraySerializable $expectedObject, string $feed)
    {
        if (!$this->isDeserializationTestable) {
            $this->expectException(DeserializationNotSupportedException::class);
            $this->serializer->deserialize($feed, $this->getSystemUnderTestClassName());
        }
        $object = $this->serializer->deserialize($feed, $this->getSystemUnderTestClassName());

        $this->assertThat($object, $this->isInstanceOf($this->getSystemUnderTestClassName()));
        $this->assertThat($object, $this->equalTo($expectedObject));
    }

    public function testItCanUseGetterAndSetter()
    {
        $reflect = new \ReflectionClass($this->getSystemUnderTestClassName());
        $properties = $reflect->getProperties();
        if (empty($properties)) {
            $this->assertTrue(true, 'no properties to test getters & setters');
        }
        foreach ($properties as $property) {
            if (in_array($property->name, $this->ignoredPropertiesForAccessorTest)) {
                continue;
            }
            if ($property->isStatic()) {
                continue;
            }
            $object = $this->buildObject();
            $accessors = $this->findAccessorNames($reflect, $property);
            $type = $property->getType()->getName();
            if ($accessors['getter'] === null && $accessors['setter'] === null) {
                $this->assertTrue(true, 'no setter or getter found');
            } elseif ($accessors['getter'] !== null) {
                $getter = $accessors['getter'];
                $value = $this->getValueForType($type);
                if ($accessors['setter'] !== null) {
                    $setter = $accessors['setter'];
                    $object->$setter($value);
                }
                // use delta for \DateTime since object creation is done a few microseconds before test in case there is no setter
                $this->assertThat($object->$getter(), new IsEqual($value, 1));
            }
        }
    }

    private function foo(string $file)
    {
        return include $file;
    }

    private function buildObject(): mixed
    {
        $class = $this->getSystemUnderTestClassName();
        $reflect = new \ReflectionClass($class);
        $parameters = [];
        $constructor = $reflect->getConstructor();
        if (!is_null($constructor)) {
            foreach ($constructor->getParameters() as $reflectionParameter) {
                $parameters[] = $this->getValueForType($reflectionParameter->getType()->getName());
            }

            return new $class(...$parameters);
        }

        return new $class();
    }

    private function getValueForType(string $type): mixed
    {
        if (!array_key_exists($type, $this->dataTypeProviders)) {
            throw new \Exception("cannot find data type provider for '$type'");
        }

        return $this->dataTypeProviders[$type]();
    }

    private function findAccessorNames(\ReflectionClass $reflect, \ReflectionProperty $reflectionProperty): ?array
    {
        $accessors = [
            'setter' => null,
            'getter' => null,
        ];
        $propertyWithUCFirst = ucfirst($reflectionProperty->name);
        $setter = "set$propertyWithUCFirst";
        if ($reflect->hasMethod($setter)) {
            $accessors['setter'] = $setter;
        }
        foreach (['get', 'is', 'has'] as $getterPrefix) {
            $getter = "$getterPrefix$propertyWithUCFirst";
            if ($reflect->hasMethod($getter)) {
                $accessors['getter'] = $getter;

                return $accessors;
            }
        }

        return $accessors;
    }

    /*****************
     * DATA PROVIDERS
     *****************/

    public function getObjectsAndFeeds(): array
    {
        $objectsAndFeeds = [];
        foreach ($this->testedModels as $testedModel) {
            $objectsAndFeeds[$testedModel] = [
                $this->getPhpVarFromFile($testedModel),
                $this->getTextFeedFromFile($testedModel),
            ];
        }

        return $objectsAndFeeds;
    }

    protected function getTextFeedFromFile(string $file): string
    {
        $path = $this->getTestFileBasePath();
        $fileName = $path.'/'.$file.'.json';
        if (is_file($fileName)) {
            return json_encode(json_decode(file_get_contents($fileName)));
        }
        throw new \RuntimeException("JSON file not found : $fileName");
    }

    protected function getSourceFeedFromFile(string $file)
    {
        $path = $this->getTestFileBasePath();
        $fileName = $path.'/'.$file;
        if (!array_key_exists($fileName, $this->includeCache)) {
            if (is_file($fileName)) {
                $object = include $fileName;
            } else {
                throw new \RuntimeException("PHP file not found : $fileName");
            }
            $this->includeCache[$fileName] = $object;
        }

        return $this->includeCache[$fileName];
    }

    protected function getPhpVarFromFile(string $file)
    {
        return $this->getSourceFeedFromFile($file.'.php');
    }
}
