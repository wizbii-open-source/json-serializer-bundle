<?php

namespace Wizbii\JsonSerializerBundle;

interface ArraySerializable
{
    public const DATETIME_FORMAT = \DateTimeInterface::ATOM;

    public function serialize(): array;

    public static function deserialize(array $contentAsArray): ArraySerializable;
}
