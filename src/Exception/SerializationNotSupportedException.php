<?php

namespace Wizbii\JsonSerializerBundle\Exception;

class SerializationNotSupportedException extends \RuntimeException implements SerializerException
{
    public function __construct(string $type, ?\Throwable $previous = null)
    {
        parent::__construct(
            "Serializer can't serialize object in JSON as this type ('$type') might not implement ArraySerializable interface.",
            SerializerException::CODE_SERIALIZATION_NOT_SUPPORTED,
            $previous
        );
    }
}
