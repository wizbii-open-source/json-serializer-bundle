<?php

namespace Wizbii\JsonSerializerBundle\Exception;

interface SerializerException
{
    public const CODE_DESERIALIZATION_NOT_SUPPORTED = 1;
    public const CODE_SERIALIZATION_NOT_SUPPORTED = 2;
    public const CODE_MISSING_MANDATORY_ATTRIBUTE = 3;
}
