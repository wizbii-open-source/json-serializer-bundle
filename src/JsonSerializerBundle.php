<?php

namespace Wizbii\JsonSerializerBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/** @codeCoverageIgnore */
class JsonSerializerBundle extends Bundle
{
    public function build(ContainerBuilder $container): void
    {
    }

    public function getPath(): string
    {
        return \dirname(__DIR__);
    }
}
