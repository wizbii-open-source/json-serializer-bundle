<?php

namespace Wizbii\JsonSerializerBundle;

use Wizbii\JsonSerializerBundle\Exception\DeserializationNotSupportedException;
use Wizbii\JsonSerializerBundle\Exception\SerializationNotSupportedException;

class Serializer
{
    public function serialize(mixed $data): string
    {
        /** @var string $content */
        $content = json_encode($this->serializeAsArray($data));

        return $content;
    }

    public function deserialize(string $dataAsString, string $type): mixed
    {
        $data = json_decode($dataAsString, true);
        if (!is_array($data)) {
            throw new DeserializationNotSupportedException($type);
        }

        return $this->deserializeFromArray($data, $type);
    }

    public function serializeAsArray(mixed $data): array
    {
        $serializedData = null;
        if (is_object($data) && $data instanceof ArraySerializable) {
            $serializedData = $this->jsonPrepareArraySerializable($data);
        } elseif (is_array($data)) {
            $serializedData = $this->jsonPrepareArray($data);
        } else {
            $type = is_object($data) ? get_class($data) : 'unknown';
            throw new SerializationNotSupportedException($type);
        }

        return $serializedData;
    }

    public function deserializeFromArray(array $data, string $type): mixed
    {
        if ($this->startsWith($type, 'array<')) {
            $returnedArray = [];
            if (!is_array($data)) {
                $data = [];
            }
            foreach ($data as $key => $value) {
                $subType = substr($type, 6, -1);
                $returnedArray[$key] = $this->deserialize((string) json_encode($value), $subType);
            }

            return $returnedArray;
        } elseif (is_a($type, ArraySerializable::class, true)) {
            /** @var callable $callable */
            $callable = [$type, 'deserialize'];
            $content = call_user_func_array($callable, [$data]);
            if (is_object($content) && $content instanceof LifecycleArraySerializable) {
                $content->postDeserialize();
            }

            return $content;
        }

        throw new DeserializationNotSupportedException($type);
    }

    private function jsonPrepareArraySerializable(ArraySerializable $data): array
    {
        if ($data instanceof LifecycleArraySerializable) {
            $data->preSerialize();
        }
        $serializedData = $data->serialize();
        if ($data instanceof LifecycleArraySerializable) {
            $data->postSerialize();
        }

        return $serializedData;
    }

    private function jsonPrepareArray(array $data): array
    {
        $serializedData = [];
        if ($this->isAssociativeArray($data)) {
            foreach ($data as $key => $value) {
                $serializedData[$key] = $this->serializeAsArray($value);
            }

            return $serializedData;
        }
        foreach ($data as $value) {
            $serializedData[] = $this->serializeAsArray($value);
        }

        return $serializedData;
    }

    private function isAssociativeArray(array $a): bool
    {
        return is_array($a) && array_keys($a) !== range(0, count($a) - 1);
    }

    private function startsWith(string $haystack, string $needle): bool
    {
        // search backwards starting from haystack length characters from the end
        return $needle === '' || strrpos($haystack, $needle, -strlen($haystack)) !== false;
    }
}
