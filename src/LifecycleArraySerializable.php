<?php

namespace Wizbii\JsonSerializerBundle;

interface LifecycleArraySerializable
{
    public function preSerialize(array $groups = []): void;

    public function postSerialize(array $groups = []): void;

    public function postDeserialize(): void;
}
